const pkg = require('./package');

module.exports = {
  mode: 'universal',
  env: {
    //serverBaseUrl: process.env.HOSTWRITER_API_BASEURL,
    // serverApiUrl: serverBaseUrl + '/jsonapi',
    // serverFilesUrl: process.env.HOSTWRITER_API_BASEURL
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },

  // router: {
  //   middleware: 'log'
  // },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@mdi/font/css/materialdesignicons.css',
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // ['nuxt-google-maps-module', {
    //   key: 'AIzaSyB-nuMPBF6wH5VqklaBv8H48_hAVIG-Kn8',
    //   libraries: [
    //     'places',
    //     'marker'
    //   ]
    // }],
    // Doc: https://github.com/nuxt-community/analytics-module
    ['@nuxtjs/google-analytics', {
      id: 'UA-79279864-1'
    }],
    // Doc: https://github.com/pimlie/nuxt-matomo
    ['nuxt-matomo', {
      matomoUrl: '//tracking.hostwriter.org/',
      siteId: 3
    }],
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios'],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  }
}
