<?php

// ======= Konfiguration:
$mailTo = 'tina@hostwriter.org';
$mailFrom = '"FormMailer" <info@hostwriter.org>';
$mailSubject = 'Ambassador Application';
$mailText = date('Y-m-d H:i:s') . ' - ' . "A new user applied for the Hostwriter ambassador program: \r\n\r\n";

// ======= Text der Mail aus den Formularfeldern erstellen:
if(isset($_POST)) {
    $post = json_decode(file_get_contents("php://input"), TRUE);
    // ob_start();
    // var_dump($data);
    // $debug_dump = ob_get_clean();
    // $mailText .= $debug_dump;

   $firstname = $post['data']['firstname'];
   $lastname = $post['data']['lastname'];
   $profileUrl = $post['data']['profileUrl'];
   $application = $post['data']['application'];

   $mailText .= "firstname: " . $firstname . "\r\n lastname: " . $lastname . "\r\n url: " . $profileUrl . "\r\n\r\n applied to be an ambassador:\r\n\r\n" . $application;
}

// ======= Korrekturen vor dem Mailversand
// Wenn PHP "Magic Quotes" vor Apostrophzeichen einfügt:
 if(get_magic_quotes_gpc()) {
   // eventuell eingefügte Backslashes entfernen
   $mailtext = stripslashes($mailtext);
 }

// ======= Mailversand
$mailSent = @mail($mailTo, $mailSubject, $mailText, "From: ".$mailFrom);
exit();

?>
